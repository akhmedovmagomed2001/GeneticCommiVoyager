from model import GuiData
from tkinter import *
from algorithm import begin


def start_algorithm(gui_data):
	algorithm_properties = gui_data.get_algorithm_properties()
	gui_data.window.destroy()
	begin(algorithm_properties)


def init_gui():
	window = Tk()

	window.title('Genetic Algorithm')
	window.geometry("500x380")
	window.resizable(width=False, height=False)

	main_title = Label(window, text="Algorithm Properties", width=20, font=("bold", 20))
	main_title.place(x=90, y=53)

	iterations_title = Label(window, text="Iterations count", width=20, font=("bold", 10))
	iterations_title.place(x=80, y=130)

	iterations_entry = Entry(window)
	iterations_entry.place(x=240, y=130)

	cities_title = Label(window, text="Cities count", width=20, font=("bold", 10))
	cities_title.place(x=80, y=180)

	cities_entry = Entry(window)
	cities_entry.place(x=240,y=180)

	mutation_title = Label(window, text="Mutation percentage", width=20, font=("bold", 10))
	mutation_title.place(x=80, y=230)

	mutation_entry = Entry(window)
	mutation_entry.place(x=240, y=230)

	start_button = Button(window, text='Start', width=20, bg='brown', fg='white')
	start_button.place(x=180, y=280)

	gui_data = GuiData(
		window=window, 
		iterations_entry=iterations_entry, 
		cities_entry=cities_entry, 
		mutation_entry=mutation_entry
	)

	start_button['command'] = lambda gui_data=gui_data : start_algorithm(gui_data)

	window.mainloop()
