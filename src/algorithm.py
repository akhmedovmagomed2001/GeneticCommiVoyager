from random import randint, sample, randrange, shuffle
from model import AlgoPlot
from math import sqrt



POPULATION_SIZE = 28



# Generate cities coordinate
# Returns two entities: 
# 1. vector of X coordinates
# 2. vector of Y coordinates 
def generate_coordinates(num_cities):
	city_points = {}
	for i in range(num_cities):
		x_value, y_value = randint(0, 1000), randint(0, 1000)
		city_points[i] = {'x': x_value, 'y': y_value}
	return city_points



# Generate initial population
# returns a list of random combination of input cities
# 'population count' number of the output combinations
def generate_population(population_count, num_cities):
	cities = list(range(0, num_cities))

	all_ways = []
	for _ in range(population_count):
		way = sample(cities, num_cities)
		all_ways.append(way)

	return all_ways



# Calculate the distance between two points
def distance(point1, point2):
	return sqrt((point2['x'] - point1['x']) ** 2 + (point2['y'] - point1['y']) ** 2)



# Calculate the distance of the path
# 'path' is a sequence of the cities like ['1', '3', '2']
# 'city_points' is a dictionary of city index and city point: {0: {'x': 3, 'y': 4}, 1: {'x': 1, 'y': 2}}
def get_path_length(city_points, path):
	if len(city_points) != len(path):
		raise ValueError("Invalid 'city_points' and 'path' length")
	
	path_length = 0
	last_city_index = len(path) - 1
	for i in path:
		last_city_point = city_points[last_city_index]
		current_city_point = city_points[i]
		path_length = path_length + distance(last_city_point, current_city_point)
		last_city_index = i

	return path_length



# Mutate some entries in the 'population'
# The count of the mutated entries is defined by 'percent_mutation'
# Returns mutated population
def mutate(population, percent_mutation):
	count_mutation = (int(len(population)) * percent_mutation) // 100

	index_to_shuffle = []
	while (len(index_to_shuffle) != count_mutation):
		random_index = randint(0, len(population) - 1)
		if random_index not in index_to_shuffle:
			index_to_shuffle.append(random_index)

	for population_index in index_to_shuffle:
		current_population = population[population_index]

		point1, point2 = -1, -1
		while point2 is point1:
			point1 = randrange(0, len(current_population))
			point2 = randrange(0, len(current_population))

		current_population[point1], current_population[point2] = current_population[point2], current_population[point1]

	return population



# Remove the most long routes from the population
def clean_population(population, distances):
	initial_population_len = len(population)
	
	while len(population) > initial_population_len / 2:
		idx = distances.index(max(distances))
		distances.pop(idx)
		population.pop(idx)

	return population



# Make cross over and get children 
def cross_over(population):
	half = len(population) // 2

	first_partners = population[:half]
	second_partners = population[half:]
	shuffle(second_partners)
	
	children = []
	for first_partner, second_partner in zip(first_partners, second_partners):

		cycle = []
		elem = first_partner[0]
		while elem not in cycle:
			index = second_partner.index(elem)
			cycle.append(elem)
			elem = first_partner[index]

		child1, child2 = [], []
		for i in range(len(second_partner)):
			i_value = first_partner[i]
			if i_value in cycle:
				child1.append(i_value)
				child2.append(second_partner[i])
			else:
				child1.append(second_partner[i])
				child2.append(i_value)

		children.append(child1)
		children.append(child2)	
	
	return children



def begin(properties):
	population = generate_population(POPULATION_SIZE, properties.cities_count)
	city_points = generate_coordinates(properties.cities_count)
	algo_plot = AlgoPlot(city_points)

	for iteration_num in range(properties.iterations_count):
		distances = []
		for path in population:
			distance = get_path_length(city_points, path)
			distances.append(distance)

		min_distance_idx = distances.index(min(distances))
		min_path = population[min_distance_idx]
		if iteration_num is not (properties.iterations_count - 1):
			algo_plot.add_path(min_path)
		else:
			algo_plot.add_path(min_path, 1)

		print('Iteration:', iteration_num + 1)
		print('Min distance:', min(distances))
		print('Min path:', min_path)
		print()

		population = clean_population(population, distances)

		children = cross_over(population)
		population = population + children

		population = mutate(population, properties.mutation_percentage)

	algo_plot.save_plot()
	algo_plot.show_plot()
	algo_plot.clean_plot()
