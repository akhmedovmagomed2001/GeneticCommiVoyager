import numpy as np

import matplotlib
matplotlib.use('TkAgg')

import matplotlib.pyplot as plt


class AlgorithmProperties:
	def __init__(self, iterations_count: int , cities_count: int, mutation_percentage: int):
		self._iterations_count = int(iterations_count)
		self._cities_count = int(cities_count)

		mutation_percentage = int(mutation_percentage)
		self.__validate_mutation_percentage(mutation_percentage)
		self._mutation_percentage = mutation_percentage

	def __str__ (self):
		output_format = "Iterations count: '{}'\nCities count: '{}'\nMutation percentage: '{}'"
		return output_format.format(self.iterations_count, self.cities_count, self.mutation_percentage)

	@staticmethod
	def __validate_mutation_percentage(mutation_percentage: int):
		if (mutation_percentage < 0) or (mutation_percentage > 100):
			message_template = "Mutation percentage has invalid value: '{}'"
			raise TypeError(message_template.format(mutation_percentage))

	@property
	def iterations_count(self):
		return self._iterations_count

	@property
	def cities_count(self):
		return self._cities_count

	@property
	def mutation_percentage(self):
		return self._mutation_percentage



class GuiData:
	def __init__(self, window, iterations_entry, cities_entry, mutation_entry):
		self._window = window
		self._iterations_entry = iterations_entry
		self._cities_entry = cities_entry
		self._mutation_entry = mutation_entry
	
	def get_algorithm_properties(self):
		return AlgorithmProperties (
			iterations_count=self._iterations_entry.get(),
			cities_count=self._cities_entry.get(),
			mutation_percentage=self._mutation_entry.get()
		)
	
	@property
	def window(self):
		return self._window



class AlgoPlot:
	def __init__(self, city_points):
		self._city_points = city_points
		self._fig, self._ax = plt.subplots(figsize=(6, 6))
		self.__initialize_plot()
	
	def add_path(self, path, is_red=0):
		x, y = [], []
		for i in range(len(self._city_points)):
			city_index = path[i]
			city_point = self._city_points[city_index]
			x.append(city_point['x'])
			y.append(city_point['y'])
		x.append(x[0])
		y.append(y[0])

		if is_red:
			self._ax.plot(x, y, color='r', linewidth=3)	
		else:
			self._ax.plot(x, y, color=np.random.rand(3, ), alpha=0.75)

	def show_plot(self):
		plt.show()

	def clean_plot(self):
		self._ax.cla()
	
	def save_plot(self):
		plt.savefig('map.pdf')
	
	def __initialize_plot(self):
		x, y = self.__convert_coordinates_matplot()
		self._ax.scatter(x=np.array(x), y=np.array(y), c='black')

		for i in range(len(self._city_points)):
			self._ax.annotate(str(i), (x[i] + 10, y[i] + 10))

	def __convert_coordinates_matplot(self):
		city_points = self._city_points
		x, y = [], []
		for key in city_points:
			point = city_points[key]
			x.append(point['x'])
			y.append(point['y'])
		return x, y
